//
//  TGFoursquareLocationDetail.h
//  TGFoursquareLocationDetail-Demo
//
//  Created by Thibault Guégan on 15/12/2013.
//  Copyright (c) 2013 Thibault Guégan. All rights reserved.
//

@import UIKit;
@import MapKit;

#import "KIImagePager.h"

typedef NS_ENUM(NSInteger, ADFoursquarePlaceDetailType)
{
    ADFoursquarePlaceDetailTypeCarousel = 1,
    ADFoursquarePlaceDetailTypeGenericView = 2,
    ADFoursquarePlaceDetailTypeMap = 3
};

@class ADFoursquarePlaceDetail, MKMapView;

@protocol ADFoursquarePlaceDetailDelegate <NSObject>

@optional

- (void)locationDetail:(ADFoursquarePlaceDetail *)locationDetail
     imagePagerDidLoad:(KIImagePager *)imagePager;

- (void)locationDetail:(ADFoursquarePlaceDetail *)locationDetail
      tableViewDidLoad:(UITableView *)tableView;

- (void)locationDetail:(ADFoursquarePlaceDetail *)locationDetail
     headerViewDidLoad:(UIView *)headerView;
@end

@interface ADFoursquarePlaceDetail : UIView <UIScrollViewDelegate>

/**
 How fast is the table view scrolling with the image picker
*/
@property (nonatomic) CGFloat parallaxScrollFactor;
@property (nonatomic) CGFloat defaultimagePagerHeight;
@property (nonatomic) CGFloat headerFade;

@property (nonatomic, strong) KIImagePager *imagePager;
@property (nonatomic, strong) MKMapView *mapView;

@property (nonatomic) int currentImage;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

/// the view that act as a replacement for the imagePager
@property (nonatomic, strong) UIView *genericView;

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIView *headerView;

@property (nonatomic, strong) UIColor *backgroundViewColor;

@property (nonatomic, weak) id <UITableViewDataSource> tableViewDataSource;
@property (nonatomic, weak) id <UITableViewDelegate> tableViewDelegate;

@property (nonatomic, weak) id <ADFoursquarePlaceDetailDelegate> delegate;

@property (nonatomic) ADFoursquarePlaceDetailType type;

@end